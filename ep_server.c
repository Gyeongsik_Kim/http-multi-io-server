#include<stdlib.h>
#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<netdb.h>
#include<string.h>
#include<sys/sendfile.h>
#include<sys/epoll.h>
#include<unistd.h>
#include<errno.h>

#define CONNMAX 1000
#define BYTES 9999
#define MAXEPOLLSIZE 1000
#define BACKLOG 200 // how many pending connections queue will hold

//http://blog.abhijeetr.com/2010/04/very-simple-http-server-writen-in-c.html

char *ROOT;
char response[] = "HTTP/1.1 200 OK\r\n"
"Conection:close\r\nContent-Type: text/html; charset=UTF-8\r\n\r\n"
"<!DOCTYPE html><html><head><title>TEST!</title>"
"<style>body { background-color: #111 }"
"h1 { font-size:4cm; text-align: center; color: black;"
" text-shadow: 0 0 2mm red}</style></head>"
"<body><h1>Goodbye, world!</h1></body></html>\r\n";

int set_non_blocking(int sockfd)
{
	int flags, s;
	flags = fcntl(sockfd, F_GETFL, 0);
	if(flags == -1)
	{
		perror("fcntl");
		return -1;
	}
	flags |= O_NONBLOCK;
	s = fcntl(sockfd, F_SETFL, flags);
	if(s == -1)
	{
		perror("fcntl");
		return -1;
	}
	return 0;
}

int main(int argc, char *argv[])
{
  
    ROOT = getenv("PWD");
    	int rcvd, fd, bytes_read;
	char re[9999], *reqline[3], data_to_send[BYTES], path[99999];
	int status;
	int sockfd, new_fd, kdpfd, nfds, n, curfds;
	struct addrinfo hints;
	struct addrinfo *servinfo; 				// will point to the results 
	struct addrinfo *p;
	struct sockaddr_storage client_addr;
	struct epoll_event ev;
	struct epoll_event *events;
	socklen_t addr_size;

	if (argc != 2) {
		fprintf(stderr, "usage: epoll web server need more arguments\n");
		return 1;
	}



	memset(&hints, 0, sizeof hints);	// make sure the struct is empty
	hints.ai_family = AF_UNSPEC; 			// don't care IPv4 or IPv6
	hints.ai_socktype = SOCK_STREAM; 	// TCP stream sockets
	hints.ai_flags = AI_PASSIVE;			// fill in my IP for me

	if((status = getaddrinfo(NULL, argv[1], &hints, &servinfo)) != 0 ) {
		fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
		return 2;
	}
	// servinfo now points to a linked list of 1 or more struct addrinfos
	for (p = servinfo; p != NULL; p = p->ai_next ) {

		// make a socket:
		if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("server: socket");
			continue;
		}

		// make the sock non blocking
		set_non_blocking(sockfd);

		// bind it to the port
		if ((bind(sockfd, p->ai_addr, p->ai_addrlen)) == -1) {
			close(sockfd);
			perror("server: bind");
			continue;
		}

		break;
	}

	if(p == NULL) {
		fprintf(stderr, "server: failed to bind\n");
		return 2; // ???????????
	}

	freeaddrinfo(servinfo); // free the linked-list

	// listen for incoming connection
	if (listen(sockfd, BACKLOG) == -1) {
		perror("listen");
		exit(1);
	}

	printf("server: waiting for connections...\n");

	kdpfd = epoll_create(MAXEPOLLSIZE);
	ev.events = EPOLLIN|EPOLLET;
	ev.data.fd = sockfd;
	if(epoll_ctl(kdpfd, EPOLL_CTL_ADD, sockfd, &ev) < 0)
	{
		fprintf(stderr, "epoll set insert error.");
		return -1;
	} else {
		printf("success insert listening socket into epoll.\n");
	}
	events = calloc (MAXEPOLLSIZE, sizeof ev);
	curfds = 1;
	while(1) 
	{ //loop for accept incoming connection
		memset(&re, '\0', sizeof(re));
		nfds = epoll_wait(kdpfd, events, curfds, -1);
		if(nfds == -1) {
			perror("epoll_wait");
			break;
		}		
		
        for (n = 0; n < nfds; ++n) {
			if(events[n].data.fd == sockfd){
				addr_size = sizeof client_addr;
				new_fd = accept(events[n].data.fd, (struct sockaddr *)&client_addr, &addr_size);
				if (new_fd == -1) {
					if((errno == EAGAIN) ||
						 (errno == EWOULDBLOCK))
						break;
					else {
						perror("accept");
						break;
					}
				}
                
				fprintf(stdout, "server: connection established...\n");
				set_non_blocking(new_fd);
				ev.events = EPOLLIN|EPOLLET;
				ev.data.fd = new_fd;
				if(epoll_ctl(kdpfd,EPOLL_CTL_ADD, new_fd, &ev)<0) 
					printf("Failed to insert socket into epoll.\n");
				
				curfds++;
			} else {
				if(recv(events[n].data.fd, re, sizeof(re), 0) <= 0){
					fprintf(stderr, "RECV ERROR\n");
					exit(EXIT_FAILURE);					
				}
				fprintf(stdout, "%s", re);
				reqline[0] = strtok(re, " \t\n");
				if(strncmp(reqline[0], "GET\0", 4) == 0){
					reqline[1] = strtok (NULL, " \t");
					reqline[2] = strtok (NULL, " \t\n");
					if ( strncmp( reqline[2], "HTTP/1.0", 8)!=0 && strncmp( reqline[2], "HTTP/1.1", 8)!=0 )
						write(events[n].data.fd, "HTTP/1.1 400 Bad Request\n", 25);
					 else {
						if ( strncmp(reqline[1], "/\0", 2)==0 )
						    reqline[1] = "/index.php";        //Because if no file is specified, index.html will be opened by default (like it happens in APACHE...

						strcpy(path, ROOT);
						strcpy(&path[strlen(ROOT)], reqline[1]);
						printf("file: %s\n", path);
						if ( (fd=open(path, O_RDONLY))!=-1 ) {
						    send(events[n].data.fd, "HTTP/1.1 200 OK\n\n", 17, 0);
						    while ( (bytes_read=read(fd, data_to_send, BYTES))>0 ){
							write (events[n].data.fd, data_to_send, bytes_read);
							}
						}else {
						  //      send(events[n].data.fd, "HTTP/1.1 200 OK\n\n", 17, 0);
						//	fd = open("404page.php", O_RDONLY);
							send(events[n].data.fd, "HTTP/1.1 404 Not Found\n\n", sizeof("HTTP/1.1 404 Not Found\n\n"), 0);
                                                  //  while ( (bytes_read=read(fd, data_to_send, BYTES))>0 )
                                                //        write (events[n].data.fd, data_to_send, bytes_read);
//							continue;
						}
						fprintf(stdout, "fd = %d\n", fd);					
					}
					epoll_ctl(kdpfd, EPOLL_CTL_DEL, events[n].data.fd, &ev);
					curfds--;
					close(events[n].data.fd);
				}
			}
		}
	}
	free(events);
	close(sockfd);			
	return 0;
}

